package com.mutineer.kissel;

import android.accounts.AbstractAccountAuthenticator;
import android.accounts.Account;
import android.accounts.AccountAuthenticatorResponse;
import android.accounts.AccountManager;
import android.accounts.NetworkErrorException;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

public class AuthenticationService extends Service {

	private static JuickAccountAuthenticator mAuthenticator = null;

	@Override
	public IBinder onBind(Intent intent) {
		IBinder ret = null;
		if(intent.getAction().equals(android.accounts.AccountManager.ACTION_AUTHENTICATOR_INTENT))
		{
			ret = getAuthenticator().getIBinder();
			Log.d("AuthService", "binding");
		}
		return ret;
	}
	
	private JuickAccountAuthenticator getAuthenticator()
	{
		if(mAuthenticator == null)
			mAuthenticator = new JuickAccountAuthenticator(this);
		return mAuthenticator;
	}
	
	public class JuickAccountAuthenticator extends AbstractAccountAuthenticator {
		private Context mContext;

		public JuickAccountAuthenticator(Context context) {
			super(context);
			mContext = context;
		}

		@Override
		public Bundle addAccount(AccountAuthenticatorResponse response,
				String accountType, String authTokenType,
				String[] requiredFeatures, Bundle options)
				throws NetworkErrorException {
			Intent i = new Intent(mContext, SignInActivity.class);
			i.putExtra(AccountManager.KEY_ACCOUNT_AUTHENTICATOR_RESPONSE, response);
			Bundle reply = new Bundle();
			reply.putParcelable(AccountManager.KEY_INTENT, i);
			return reply;
		}

		@Override
		public Bundle confirmCredentials(AccountAuthenticatorResponse response,
				Account account, Bundle options) throws NetworkErrorException {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public Bundle editProperties(AccountAuthenticatorResponse response,
				String accountType) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public Bundle getAuthToken(AccountAuthenticatorResponse response,
				Account account, String authTokenType, Bundle options)
				throws NetworkErrorException {
			Log.d("Auth", "Token requested");
			return null;
		}

		@Override
		public String getAuthTokenLabel(String authTokenType) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public Bundle hasFeatures(AccountAuthenticatorResponse response,
				Account account, String[] features)
				throws NetworkErrorException {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public Bundle updateCredentials(AccountAuthenticatorResponse response,
				Account account, String authTokenType, Bundle options)
				throws NetworkErrorException {
			// TODO Auto-generated method stub
			return null;
		}

	}

}
