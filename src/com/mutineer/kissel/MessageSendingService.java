package com.mutineer.kissel;

import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.os.Vibrator;
import android.util.Log;
import android.widget.RemoteViews;
import android.widget.Toast;

public class MessageSendingService extends IntentService {
	public static final String INTENT_EXTRA_BODY = "MessageBody";
	public static final String INTENT_EXTRA_SUCCESS_MESSAGE = "SuccessMessage";
	public static final String INTENT_EXTRA_ATTCHMENT_URI = "AttachmentUri";
	
	private static final int TOAST_MESSAGE = 1;
	private static final int PROGRESS_MESSAGE = 2;
	
	private Handler handler;
	private Notification SendingNotification = null;
	private NotificationManager nm = null;
	
	@Override
	public void onCreate() {
		handler = new Handler() {

			@Override
			public void handleMessage(Message msg) {
				super.handleMessage(msg);
				switch (msg.what) {
				case TOAST_MESSAGE:
					Toast.makeText(getApplicationContext(), msg.arg1, Toast.LENGTH_SHORT).show();
					break;
					
				case PROGRESS_MESSAGE:
					SendingNotification.contentView.setProgressBar(R.id.sending_progress_bar, msg.arg1, msg.arg2, false);
					SendingNotification.contentView.setTextViewText(R.id.sending_progress_text, 
																	String.format("%,d / %,d B", msg.arg2, msg.arg1));
					nm.notify(KisselApplication.NOTIFICATION_SENDING, SendingNotification);
					break;

				default:
					break;
				}
			}
			
		};
		
		nm = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
		super.onCreate();
	}

	public MessageSendingService() {
		super("SendingService");
	}
	
	private boolean sendMessage(String message, String attachmentUri)
	{
		try {
			final String end = "\r\n";
			final String twoHyphens = "--";
			final String boundary = "****+++++******+++++++********";
			
			URL apiUrl = new URL("http://api.juick.com/post");
			HttpURLConnection conn = (HttpURLConnection) apiUrl.openConnection();
			conn.setConnectTimeout(10000);
			conn.setDoOutput(true);
			conn.setUseCaches(false);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Connection", "Keep-Alive");
			conn.setRequestProperty("Charset", "UTF-8");
			conn.setRequestProperty("Authorization", Utils.getAuthToken(this));
			conn.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + boundary);
			
			String outStr = twoHyphens + boundary + end;
			outStr += "Content-Disposition: form-data; name=\"body\"" + end + end + message + end;
			
			if(attachmentUri != null && attachmentUri.length() > 0) {
				String fname = "file.jpg";
				outStr += twoHyphens + boundary + end;
                outStr += "Content-Disposition: form-data; name=\"attach\"; filename=\"" + fname + "\"" + end + end;
			}
			
			byte[] outStrB = outStr.getBytes("UTF-8");
			
			String outStrEnd = twoHyphens + boundary + twoHyphens + end;
            byte outStrEndB[] = outStrEnd.getBytes();
            
            int size = outStrB.length + outStrEndB.length;
            int fsize = 0;
            
            FileInputStream fileInput = null;
            if(attachmentUri != null && attachmentUri.length() > 0) {
            	fileInput = getContentResolver().openAssetFileDescriptor(Uri.parse(attachmentUri), "r").createInputStream();
            	fsize = fileInput.available();
            	size += fsize + end.length();
            }
            
            conn.setFixedLengthStreamingMode(size);
            conn.connect();
            BufferedOutputStream out = new BufferedOutputStream(conn.getOutputStream());
            out.write(outStrB);
            
            if(attachmentUri != null && attachmentUri.length() > 0 && fileInput != null) {
            	byte[] buffer = new byte[4096];
            	int length = -1;
            	int total = 0;
            	int totallast = 0;
            	while ((length = fileInput.read(buffer, 0, 4096)) != -1) {
					out.write(buffer, 0, length);
					total += length;
					if(((int) total / 25600) != totallast) {
						totallast = (int)(total / 25600);
						Message.obtain(handler, PROGRESS_MESSAGE, fsize, total).sendToTarget();
					}
				}
            	out.write(end.getBytes());
            	fileInput.close();
            	Message.obtain(handler, PROGRESS_MESSAGE, fsize, fsize).sendToTarget();
            }
            
            out.write(outStrEndB);
            out.flush();
            out.close();
            
            return conn.getResponseCode() == 200;
		} catch (Exception e) {
			Log.e("Sending message", e.toString());
		}
		return false;
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		if(intent.hasExtra(MessageSendingService.INTENT_EXTRA_BODY)) {
			String attachmentUri = null;
			String message = intent.getStringExtra(INTENT_EXTRA_BODY);
			int result_message = intent.getIntExtra(INTENT_EXTRA_SUCCESS_MESSAGE, 
												    R.string.Message_posted);
			
			if(intent.hasExtra(INTENT_EXTRA_ATTCHMENT_URI))
				attachmentUri = intent.getStringExtra(INTENT_EXTRA_ATTCHMENT_URI);
			
			SendingNotification = new Notification(R.drawable.ic_stat_notify_sending, 
				  								   getText(R.string.Sending___), 
				  								   System.currentTimeMillis());
			
			SendingNotification.contentIntent = PendingIntent.getActivity(this, 0, new Intent(), 0);
			SendingNotification.contentView = new RemoteViews(getPackageName(), R.layout.sending_notif_layout);
			SendingNotification.contentView.setTextViewText(R.id.message_text, message);
			SendingNotification.contentView.setImageViewResource(R.id.notif_icon, R.drawable.ic_stat_notify_sending);
			SendingNotification.contentView.setProgressBar(R.id.sending_progress_bar, 100, 0, true);
			startForeground(KisselApplication.NOTIFICATION_SENDING, SendingNotification);
			
			if(!sendMessage(message, attachmentUri))
			{
				((Vibrator) getSystemService(VIBRATOR_SERVICE)).vibrate(300);
				NotificationManager nm = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
				
				Intent resendIntent = new Intent(this, NewMessageActivity.class);
				resendIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				resendIntent.putExtra(MessageSendingService.INTENT_EXTRA_BODY, message);
				if(attachmentUri != null)
					resendIntent.putExtra(INTENT_EXTRA_ATTCHMENT_URI, attachmentUri);
				
				Notification FailNotification = new Notification(R.drawable.ic_stat_notify_sending_fail,
																 getText(R.string.Sending_problem),
																 System.currentTimeMillis());
				FailNotification.setLatestEventInfo(this, 
													getText(R.string.Sending_problem), 
													message, 
													PendingIntent.getActivity(this, 0, resendIntent, PendingIntent.FLAG_UPDATE_CURRENT));
				FailNotification.flags |= Notification.FLAG_AUTO_CANCEL;
				
				nm.notify(KisselApplication.NOTIFICATION_SEND_FAILS, FailNotification);
				
				result_message = R.string.Error;
			}
			Message.obtain(handler, TOAST_MESSAGE, result_message, 0).sendToTarget();
			
			stopForeground(true);
		}
	}

}
