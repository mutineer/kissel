package com.mutineer.kissel;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

import org.apache.http.util.ByteArrayBuffer;

public class WsClient {
	
	private static final byte keepAlive[] = {(byte)0x00, (byte)0x20, (byte)0xFF};
	private Socket sock;
	private InputStream is;
	private OutputStream os;
	WsClientListener listener = null;
	
	public void setListener(WsClientListener listener) {
		this.listener = listener;
	}
	
	public boolean connect(String host, int port, String location, String headers) {
		try {
			sock = new Socket(host, port);
			is = sock.getInputStream();
			os = sock.getOutputStream();
			
			String handshake = "GET " + location + " HTTP/1.1\r\n" +
                    		   "Host: " + host + "\r\n" +
                    		   "Connection: Upgrade\r\n" +
                    		   "Upgrade: WebSocket\r\n" +
                    		   "Origin: http://juick.com/\r\n" +
                    		   "Sec-WebSocket-Key1: 9 9 9 9\r\n" +
                    		   "Sec-WebSocket-Key2: 8 8 8 8 8\r\n" +
                    		   "Sec-WebSocket-Protocol: sample\r\n";
			if(headers != null)
				handshake += headers;
			handshake += "\r\n12345678";
			os.write(handshake.getBytes());
			
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			sock = null;
			is = null;
			os = null;
			return false;
		}
	}
	
	public boolean isConnected() {
		return (sock != null) && sock.isConnected();
	}
	
	public void sendTextFrame(String str) throws IOException {
		if (isConnected() && os != null) {
			int len = str.getBytes().length;
			byte buf[] = new byte[len + 2];
			buf[0] = 0x00;
			System.arraycopy(str.getBytes(), 0, buf, 1, len);
			buf[len + 1] = (byte) 0xFF;
			os.write(buf);
		}
	}
	
	public void sendKeepAlive() {
		if(isConnected() && os != null) {
			try {
				os.write(keepAlive);
				os.flush();
			} catch (IOException e) {
				e.printStackTrace();
			}	
		}
	}
	
	public void readLoop() {
		if(isConnected() && is != null) {
			try {
				int b;
				ByteArrayBuffer buf = new ByteArrayBuffer(16);
				boolean flagInside = false;
				while ((b = is.read()) != -1) {
					if(b == 0x00 && !flagInside) {
						buf.clear();
						flagInside = true;
					} else if(b == 0xFF && flagInside) {
						if(listener != null)
							listener.onWebSocketTextFrame(new String(buf.toByteArray(), "utf-8"));
						flagInside = false;
					} else if(flagInside) {
						buf.append((char) b); 
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}	
	
	public interface WsClientListener {
		public void onWebSocketTextFrame(String data) throws IOException;
	}
}
