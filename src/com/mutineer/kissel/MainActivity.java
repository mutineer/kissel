package com.mutineer.kissel;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ListActivity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.InputType;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.CursorAdapter;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.mutineer.kissel.api.JuickMessage;

public class MainActivity extends ListActivity implements OnClickListener,
														  OnItemClickListener{

	private static final int SIGNIN_ACTIVITY = 1;
	private static final int THREAD_NUMBER_DIALOG = 2;
	private Button bTitleWrite;
	private Button bTitleHome;
	private ProgressBar pbUpdateProgress;
	
	private SQLiteDatabase db;
	private Cursor cursor;
	
	private IntentFilter filter;
	private HomelineReceiver receiver;
	private KisselApplication application;

	/** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
    	try {
    		requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
    	} catch (Exception e) {
		}
    	
        super.onCreate(savedInstanceState);
        
        if(filter == null) {
        	filter = new IntentFilter(KisselApplication.ACTION_NEW_MESSAGES);
        	filter.addAction(KisselApplication.ACTION_UPDATE_IN_PROGRESS);
        	receiver = new HomelineReceiver();
        	application = (KisselApplication) getApplication();
        }

        if(!Utils.hasAuth(this)) {
        	startActivityForResult(new Intent(this, SignInActivity.class), SIGNIN_ACTIVITY);
        	return;
        } 
        
        Intent intent = getIntent();
        if(intent != null) {
        	Uri uri = intent.getData();
        	if(uri != null && !uri.getPathSegments().isEmpty() && parseUri(uri)) {
        		return;
        	}
        }
        
        setListAdapter(null);
		
		getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.main_title);
		
		bTitleWrite = (Button) findViewById(R.id.buttonTitleWrite);
        bTitleHome  = (Button) findViewById(R.id.buttonTitleHome);
        //bTitleHome.setCompoundDrawablesWithIntrinsicBounds(R.drawable.main_title_switch, 0, 0, 0);
        bTitleWrite.setOnClickListener(this);
        bTitleHome.setOnClickListener(this);
        pbUpdateProgress = (ProgressBar) findViewById(R.id.updateProgressBar);
        
        getListView().setOnItemClickListener(this);
        getListView().setOnItemLongClickListener(new MessageMenu(this));
        getListView().setSmoothScrollbarEnabled(false);
    }
    
	@Override
	protected void onDestroy() {
		super.onDestroy();
		
		if(cursor != null)
			cursor.close();
		if(db != null)
			db.close();
	}

	@Override
	protected void onResume() {
		super.onResume();
		
		application.setMainActivityStatus(true);
		new DatabaseQuery().execute();
		registerReceiver(receiver, filter);
	}

	@Override
	protected void onPause() {
		super.onPause();
		
		application.setMainActivityStatus(false);
		unregisterReceiver(receiver);
	}

	@Override
	protected Dialog onCreateDialog(int id, Bundle args) {
		Dialog dialog = null;
		switch(id) {
		case THREAD_NUMBER_DIALOG:
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setTitle("Enter message id:");
			
			final EditText etMessageNumber = new EditText(this);
			etMessageNumber.setInputType(InputType.TYPE_CLASS_NUMBER);
			builder.setView(etMessageNumber);
			builder.setNegativeButton(R.string.Cancel, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					dialog.cancel();
				}
			});
			builder.setPositiveButton(R.string.OK, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					Intent intent = new Intent(MainActivity.this, ThreadActivity.class);
					try {
						intent.putExtra(KisselApplication.EXTRA_MID, Integer.parseInt(etMessageNumber.getText().toString()));
						startActivity(intent);
					} catch (Exception e) {}
					etMessageNumber.setText("");
					dialog.dismiss();
				}
			});
			dialog = builder.create();
			break;
		}
		return dialog;
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if(requestCode == SIGNIN_ACTIVITY) {
			if(resultCode == RESULT_OK) {
				onCreate(new Bundle());
			} else {
				finish();
			}
		}
	}

	public void onClick(View v) {
		if(v == bTitleWrite) {
			startActivity(new Intent(this, NewMessageActivity.class));
		} else if(v == bTitleHome) {
		}
	}
	
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		JuickMessage message = DBHelper.getMessage((Cursor) parent.getItemAtPosition(position));
		Intent intent = new Intent(this, ThreadActivity.class);
		intent.putExtra(KisselApplication.EXTRA_MID, message.MID);
		startActivity(intent);
	}
	
    @Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater mi = getMenuInflater();
		mi.inflate(R.menu.main_menu, menu);
		return true;
	}
    
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId())
		{
		case R.id.menuPrefs:
			startActivity(new Intent(this, KisselPreferenceActivity.class));
			break;
			
		case R.id.menuRefresh:
			startService(new Intent(this, HomeUpdateService.class));
			break;
		case R.id.menuGoTo:
			showDialog(THREAD_NUMBER_DIALOG, null);
			break;
		}
		return true;
	}
	
	private boolean parseUri(Uri uri) {
		List<String> segs = uri.getPathSegments();
		if(segs.size() == 1 && segs.get(0).matches("\\A[0-9]+\\z") ||
			segs.size() == 2 && segs.get(1).matches("\\A[0-9]+\\z") && !segs.get(0).equals("places")) {
				int mid = Integer.parseInt(segs.get(segs.size() - 1));
				if(mid > 0) {
					finish();
					Intent intent = new Intent(this, ThreadActivity.class);
					intent.putExtra(KisselApplication.EXTRA_MID, mid);
					intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(intent);
					return true;
				}
			}
		
		return false;
	}
	
	class HomelineReceiver extends BroadcastReceiver
	{
		@Override
		public void onReceive(Context context, Intent intent) {
			if(intent.getAction().equals(KisselApplication.ACTION_NEW_MESSAGES)) {
				int num = intent.getIntExtra(KisselApplication.EXTRA_NEW_MESSAGES_COUNT, 0);
				pbUpdateProgress.setVisibility(ProgressBar.GONE);
				
				if(num > 0)
					new DatabaseQuery().execute();
			} else if(intent.getAction().equals(KisselApplication.ACTION_UPDATE_IN_PROGRESS)) {
				pbUpdateProgress.setVisibility(ProgressBar.VISIBLE);
			}
		}
	}
	
	class DatabaseQuery extends AsyncTask<Void, Void, Void>
	{ 
		@Override
		protected Void doInBackground(Void... params) {
			if(db == null || !db.isOpen()) {
				db = application.getDBHelper().getReadableDatabase();
			}
			
			cursor = db.query(DBHelper.HOMELINE_TABLE, null, null, null, null, null, DBHelper.C_APPEARANCE + " DESC");
			return null;			
		}

		@Override
		protected void onPostExecute(Void arg) {
			super.onPostExecute(arg);
			CursorAdapter adapter = (CursorAdapter) getListAdapter();
			if(adapter == null) {
				adapter = new HomelineAdapter(MainActivity.this, cursor);
				setListAdapter(adapter);
			} else {
				adapter.changeCursor(cursor);
			}
		}
	}
}

class HomelineAdapter extends CursorAdapter
{
	private int C_USER_INDEX;
	private int C_TEXT_INDEX;
	private int C_TIMESTAMP_INDEX;
	private int C_TAGS_INDEX;
	private int C_PHOTO_INDEX;
	private int C_LINKS_INDEX;
	private int C_NOT_IN_FRIENDS_INDEX;
	private DateFormat df;
	private Date dt;

	public HomelineAdapter(Context context, Cursor c) {
		super(context, c);
		getColumnIndexes(c);
		
		df = new SimpleDateFormat("HH:mm dd/MMM/yy");
        df.setTimeZone(TimeZone.getDefault());
        dt = new Date();
	}
	
	static class ViewHolder
	{
		public TextView mUser;
		public TextView mTags;
		public TextView mPhoto;
		public TextView mText;
		public TextView mTimestamp;
	}

	@Override
	public void bindView(View view, Context context, Cursor cursor) {
		ViewHolder holder = (ViewHolder) view.getTag();
		CharSequence data;
		data = "@" + cursor.getString(C_USER_INDEX);
		holder.mUser.setText(data);
		
		data = cursor.getString(C_TAGS_INDEX);
		if(data.equals("")) {
			holder.mTags.setVisibility(View.GONE);
		} else {
			holder.mTags.setVisibility(View.VISIBLE);
			holder.mTags.setText(data);
		}
		
		data = cursor.getString(C_PHOTO_INDEX);
		if(data.equals("")) {
			holder.mPhoto.setVisibility(View.GONE);
		} else {
			holder.mPhoto.setVisibility(View.VISIBLE);
			SpannableString photoUrl = new SpannableString(data);
			photoUrl.setSpan(new UnderlineSpan(), 0, data.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
			holder.mPhoto.setText(photoUrl);
		}
		
		holder.mText.setText(Utils.highlightLinksInText(cursor.getString(C_TEXT_INDEX), cursor.getString(C_LINKS_INDEX).split(" ")));
		 
        dt.setTime(cursor.getLong(C_TIMESTAMP_INDEX));
		holder.mTimestamp.setText(df.format(dt));
		
		if(cursor.getInt(C_NOT_IN_FRIENDS_INDEX) == 0)
			view.setBackgroundResource(0);
		else
			view.setBackgroundResource(R.drawable.home_recomend_item_background);
	}

	@Override
	public View newView(Context context, Cursor cursor, ViewGroup parent) {
		ViewHolder holder = new ViewHolder();
		LayoutInflater inflater = ((Activity)context).getLayoutInflater();
		View view = inflater.inflate(R.layout.home_message_row, null, true);
		holder.mUser 	  = (TextView) view.findViewById(R.id.Username);
		holder.mTags	  = (TextView) view.findViewById(R.id.Tags);
		holder.mText 	  = (TextView) view.findViewById(R.id.Text);
		holder.mTimestamp = (TextView) view.findViewById(R.id.Timestamp);
		holder.mPhoto	  = (TextView) view.findViewById(R.id.Photo);
		view.setTag(holder);
		
		return view;
	}

	@Override
	public void changeCursor(Cursor cursor) {
		getColumnIndexes(cursor);
		super.changeCursor(cursor);
	}
	
	private void getColumnIndexes(Cursor c) {
		C_USER_INDEX 	       = c.getColumnIndex(DBHelper.C_USER);
		C_TEXT_INDEX 	       = c.getColumnIndex(DBHelper.C_TEXT);
		C_TIMESTAMP_INDEX      = c.getColumnIndex(DBHelper.C_CREATED_AT);
		C_TAGS_INDEX 	       = c.getColumnIndex(DBHelper.C_TAGS);
		C_PHOTO_INDEX 	       = c.getColumnIndex(DBHelper.C_PHOTO);
		C_LINKS_INDEX	       = c.getColumnIndex(DBHelper.C_LINKS);
		C_NOT_IN_FRIENDS_INDEX = c.getColumnIndex(DBHelper.C_NOT_IN_FRIENDS);
	}
}