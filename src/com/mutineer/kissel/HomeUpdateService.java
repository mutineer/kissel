package com.mutineer.kissel;

import java.util.Calendar;
import java.util.HashSet;
import java.util.Vector;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.IntentService;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.mutineer.kissel.api.JuickMessage;
import com.mutineer.kissel.api.JuickUser;

public class HomeUpdateService extends IntentService {
    private DBHelper helper;
    private static final String homeApiUrl = "http://api.juick.com/home?1=1";
    private static final String friendsApiUrl = "http://api.juick.com/users/friends";
    
    static final String FRIENDS_UPDATE_KEY = "FriendsUpdateDate";

    public HomeUpdateService() {
        super("HomeUpdateService");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        helper = ((KisselApplication) getApplication()).getDBHelper();
    }
    
    private void updateFriendsList() {
        Calendar today = Calendar.getInstance();
        
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
        Calendar prevUpdate = Calendar.getInstance();
        prevUpdate.setTimeInMillis(sp.getLong(FRIENDS_UPDATE_KEY, 0));
        
        if( today.get(Calendar.YEAR)        != prevUpdate.get(Calendar.YEAR) ||
            today.get(Calendar.DAY_OF_YEAR) != prevUpdate.get(Calendar.DAY_OF_YEAR)) {
            
            Vector<JuickUser> users = new Vector<JuickUser>();
            final String jsonStr = Utils.getJSON(this, friendsApiUrl);
            
            try {
                JSONArray array = new JSONArray(jsonStr);
                int num = array.length();
                for(int i = 0; i < num; ++i) {
                    users.add(JuickUser.parseJSON(array.getJSONObject(i)));
                }
            
                helper.saveFriendsList(users);
                
                SharedPreferences.Editor editor = sp.edit();
                editor.putLong(FRIENDS_UPDATE_KEY, today.getTimeInMillis());
                editor.commit();
            }
            catch (Exception e) {
                Log.d("FriendsUpdate", e.toString());
            }
        }
    }
    
    private void markAndFixRecomendations(Vector<JuickMessage> messages) {
        HashSet<Integer> FriendsIDs = helper.getFriendsIDs();
        //Calendar calendar = Calendar.getInstance();
        
        for(int i = 0; i < messages.size(); ++i) {
            if(!FriendsIDs.contains(messages.get(i).UID))
                messages.get(i).notFromFriends = true;
        }
        
        /*for(int i = messages.size() - 2; i >= 0; --i) {
            Log.d("Fixer", String.valueOf(messages.get(i).MID) + " " + String.valueOf(messages.get(i).fromRecomendation));
            if(messages.get(i).fromRecomendation) {
                Log.d("Fixer", messages.get(i).Appearance.toString() + " " + messages.get(i+1).Appearance.toString());
                calendar.setTime(messages.get(i+1).Appearance);
                calendar.add(Calendar.MILLISECOND, 1);
                messages.get(i).Appearance = calendar.getTime();
                Log.d("Fixer", messages.get(i).Appearance.toString() + " " + messages.get(i+1).Appearance.toString());
            }
        }*/
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Log.d("Updater", "Update");
        
        int nNewMessages = 0;

        Intent inProgress = new Intent(KisselApplication.ACTION_UPDATE_IN_PROGRESS);
        sendStickyBroadcast(inProgress);

        updateFriendsList();
        
        Vector<JuickMessage> messages = new Vector<JuickMessage>();
        final String jsonStr = Utils.getJSON(this, homeApiUrl);
        
        try {
            JSONArray array = new JSONArray(jsonStr);
            int num = array.length();
            for(int i = 0; i < num; i++) {
                messages.add(JuickMessage.parseJSON(array.getJSONObject(i)));
            }
        }
        catch (Exception e) {
            Log.d("HomeUpdate", e.toString());
        }
        
        markAndFixRecomendations(messages);
        nNewMessages += helper.addMessages(messages);
        
        removeStickyBroadcast(inProgress);
        
        if(nNewMessages > 0) {
            KisselApplication application = (KisselApplication) getApplication();
            helper.deleteOldMessages();
            application.notifyNewMessages(nNewMessages);
        }
        
        Intent broadcast = new Intent(KisselApplication.ACTION_NEW_MESSAGES);
        broadcast.putExtra(KisselApplication.EXTRA_NEW_MESSAGES_COUNT, nNewMessages);
        sendBroadcast(broadcast);
    }

}
