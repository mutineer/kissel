package com.mutineer.kissel.api;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class JuickMessage {
	public int MID = 0;
	public int RID = 0;
	public int UID = 0;
	public String UserName = null;
	public String Text = null;
	public Date Timestamp = null;
	public Date Appearance = null;
	public String Tags = null;
	public String Photo = null;
	public String Links = null;
	public boolean notFromFriends = false;
	
	private static Pattern urlPattern = Pattern.compile("((?<=\\A)|(?<=\\s))(ht|f)tps?://[a-z0-9\\-\\.]+[a-z]{2,}/?[^\\s\\n]*", Pattern.CASE_INSENSITIVE);
	private static Pattern msgPattern = Pattern.compile("#[0-9]+");
	
	public static JuickMessage parseJSON(JSONObject json) throws JSONException {
		JuickMessage msg = new JuickMessage();
		
		msg.MID = json.getInt("mid");
		if(json.has("rid"))
			msg.RID = json.getInt("rid");
		msg.Text = json.getString("body").replace("&quot;", "\"");
		msg.UserName = json.getJSONObject("user").getString("uname");
		msg.UID = json.getJSONObject("user").getInt("uid");
		
		if(json.has("tags")) {
			JSONArray tags = json.getJSONArray("tags");
			StringBuilder tagsBuilder = new StringBuilder();
			for(int i = 0; i < tags.length(); i++) {
				tagsBuilder.append("*").append(tags.getString(i).replace("&quot;", "\""));
				tagsBuilder.append(" ");
			}
			tagsBuilder.deleteCharAt(tagsBuilder.length() - 1);
			msg.Tags = tagsBuilder.toString();
		} else {
			msg.Tags = "";
		}
		
		try {
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            df.setTimeZone(TimeZone.getTimeZone("UTC"));
            msg.Timestamp = df.parse(json.getString("timestamp"));
            msg.Appearance = msg.Timestamp;
		}
		catch(ParseException e){}
		
		if(json.has("photo")) {
			msg.Photo = json.getJSONObject("photo").getString("small");
		} else {
			msg.Photo = "";
		}
		
		//Parse for url's
		StringBuilder urlBuilder = new StringBuilder();
		Matcher m = urlPattern.matcher(msg.Text);
		while(m.find()) {
			urlBuilder.append(m.start()).append(":");
			urlBuilder.append(m.end()).append(" ");
		}
		
		//Parse for messages
		m = msgPattern.matcher(msg.Text);
		while(m.find()) {
			urlBuilder.append(m.start()).append(":");
			urlBuilder.append(m.end()).append(" ");
		}
		
		if(urlBuilder.length() > 0) {
			urlBuilder.deleteCharAt(urlBuilder.length() - 1);
			msg.Links = urlBuilder.toString();
		} else {
			msg.Links = "";
		}
		
		return msg;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		
		if(UserName != null) {
			sb.append("@").append(UserName).append(":\n");
		}
		if(Tags.length() != 0) {
			sb.append(Tags).append("\n");
		}
		if(Photo != null) {
			sb.append(Photo).append("\n");
		}
		if(Text != null) {
			sb.append(Text).append("\n");
		}
		sb.append("#").append(MID);
		if(RID != 0)
			sb.append("/").append(RID);
		sb.append(" http://juick.com/").append(MID);
		if(RID != 0)
			sb.append("#").append(RID);
		
		return sb.toString();
	}
}
