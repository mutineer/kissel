package com.mutineer.kissel.api;

import org.json.JSONException;
import org.json.JSONObject;

public class JuickUser {
	public int UID = 0;
	public String UName = null;
	
	public static JuickUser parseJSON(JSONObject json) throws JSONException {
		JuickUser user = new JuickUser();
		user.UID = json.getInt("uid");
		user.UName = json.getString("uname");
		return user;
	}
}
