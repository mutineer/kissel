package com.mutineer.kissel;

import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;

public class KisselPreferenceActivity extends PreferenceActivity implements OnSharedPreferenceChangeListener {
	Preference Refresh;
	String     RefreshKey;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    addPreferencesFromResource(R.xml.prefs);
	    
	    SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
	    sp.registerOnSharedPreferenceChangeListener(this);
	    
	    RefreshKey = getString(R.string.Prefs_id_refresh);
	    Refresh = findPreference(RefreshKey);
	    Refresh.setSummary(getRefreshTitle(sp.getString(RefreshKey, "")));
	}

	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences,
			String key) {
		if(key.equals(RefreshKey)) {
			String interval =  sharedPreferences.getString(key, "0");
			long intervalMsecs = Long.parseLong(interval) * 60 * 1000;
			
			if(intervalMsecs != 0) {
				Utils.startCheckingUpdates(this, intervalMsecs);
			} else {
				Utils.stopCheckingUpdates(this);
			}
			
			Refresh.setSummary(getRefreshTitle(interval));
		}
	}
	
	private String getRefreshTitle(String value) {
		String[] values = getResources().getStringArray(R.array.prefsRefreshIntervalValues);
		String[] titles = getResources().getStringArray(R.array.prefsRefreshIntervalTitles);
		for(int i = 0; i < values.length; i++) {
			if(value.equals(values[i]))
				return titles[i];
		}
		return "";
	}

}
