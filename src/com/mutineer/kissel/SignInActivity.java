package com.mutineer.kissel;

import java.net.HttpURLConnection;
import java.net.URL;

import android.accounts.Account;
import android.accounts.AccountAuthenticatorResponse;
import android.accounts.AccountManager;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class SignInActivity extends Activity implements OnClickListener {
	private EditText etNick;
	private EditText etPassword;
	private Button   bSave;
	private Button   bCancel;
	private Handler wrongCredsHandler = new Handler() {
		
		public void handleMessage(Message msg)
		{
			Toast.makeText(SignInActivity.this, R.string.Unknown_nick_or_wrong_password, Toast.LENGTH_LONG).show();
		}
		
	};
	
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.signin);
		
		etNick = (EditText) findViewById(R.id.juickNick);
		etPassword = (EditText) findViewById(R.id.juickPassword);
		bSave = (Button) findViewById(R.id.buttonSave);
		bCancel = (Button) findViewById(R.id.buttonCancel);
		
		bSave.setOnClickListener(this);
		bCancel.setOnClickListener(this);
		
		if(Utils.hasAuth(this))
		{
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setCancelable(false);
			builder.setNeutralButton(R.string.OK, new DialogInterface.OnClickListener() {
				
				public void onClick(DialogInterface dialog, int which) {
					SignInActivity.this.setResult(RESULT_CANCELED);
					SignInActivity.this.finish();
				}
			});
			
			builder.setMessage(R.string.Only_one_account);
			builder.show();
		}
	}

	public void onClick(View v) {
		if(v == bCancel)
		{
			setResult(RESULT_CANCELED);
			finish();
			return;
		}
		
		final String nickname = etNick.getText().toString();
		final String password = etPassword.getText().toString();
		
		if(nickname.length() == 0 || password.length() == 0)
		{
			Toast.makeText(this, R.string.Enter_nick_and_password, Toast.LENGTH_SHORT).show();
			return;
		}
		
		Toast.makeText(this, R.string.Please_wait___, Toast.LENGTH_SHORT).show();
		
		Thread checkCredsThread = new Thread(new Runnable() {
			
			public void run() {
				int status = 0;
				try
				{
					String authString = nickname + ":" + password;
					String basicAuth  = "Basic " + Base64.encodeToString(authString.getBytes(), Base64.NO_WRAP);
					
					URL apiUrl = new URL("http://api.juick.com/post");
					HttpURLConnection conn = (HttpURLConnection) apiUrl.openConnection();
					conn.setConnectTimeout(10000);
					conn.setUseCaches(false);
					conn.setRequestMethod("POST");
					conn.setRequestProperty("Authorization", basicAuth);
					conn.connect();
					status = conn.getResponseCode();
					conn.disconnect();
				}
				catch (Exception e) {
					Log.e("Login", e.toString());
				}
				if(status == 400)
				{
					Account account = new Account(nickname, getString(R.string.account_type));
					AccountManager am = AccountManager.get(SignInActivity.this);
					boolean accountCreated = am.addAccountExplicitly(account, password, null);
					Bundle extras = getIntent().getExtras();
					if(accountCreated && extras != null)
					{
						AccountAuthenticatorResponse response = extras.getParcelable(AccountManager.KEY_ACCOUNT_AUTHENTICATOR_RESPONSE);
						Bundle result = new Bundle();
						result.putString(AccountManager.KEY_ACCOUNT_NAME, nickname);
						result.putString(AccountManager.KEY_ACCOUNT_TYPE, getString(R.string.account_type));
						response.onResult(result);
					}

					startService(new Intent(SignInActivity.this, HomeUpdateService.class));
					setResult(RESULT_OK);
					finish();
				}
				else
				{
					wrongCredsHandler.sendEmptyMessage(0);
				}
			}
		});
		checkCredsThread.start();
	}

}
