package com.mutineer.kissel;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

public class NewMessageActivity extends Activity implements OnClickListener, DialogInterface.OnClickListener{
	private static final int REQUEST_ATTACH_IMAGE = 1;
	
	private String attachmentUri = null;
	private String attachmentMime = null;
	
	private EditText    etMessageBody;
	private Button   	btSendMessage;
	private ImageButton btAttach;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.new_message);
		
		etMessageBody = (EditText) findViewById(R.id.editMessageBody);
		btSendMessage = (Button) findViewById(R.id.buttonSend);
		btAttach	  = (ImageButton) findViewById(R.id.buttonAttach);
		
		btSendMessage.setOnClickListener(this);
		btAttach.setOnClickListener(this);
		
		handleIntent(getIntent());
	}
	
	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
		if(attachmentUri == null && savedInstanceState.containsKey(MessageSendingService.INTENT_EXTRA_ATTCHMENT_URI)) {
			setAttachment(savedInstanceState.getString(MessageSendingService.INTENT_EXTRA_ATTCHMENT_URI));
		}
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		if(attachmentUri != null)
			outState.putString(MessageSendingService.INTENT_EXTRA_ATTCHMENT_URI, attachmentUri);
	}

	private void handleIntent(Intent intent)
	{
		String messageText = null;
		if(intent == null)
			return;
		
		if(intent.getAction() != null && intent.getAction().equals(Intent.ACTION_SEND)) {
			String mime = intent.getType();
			if(mime == null || mime.equals("text/plain")) {
				messageText = intent.getStringExtra(Intent.EXTRA_TEXT);
			} else if(mime.equals("image/jpeg")) {
				attachmentUri = intent.getExtras().get(Intent.EXTRA_STREAM).toString();
			}
		} else {
			if(intent.hasExtra(MessageSendingService.INTENT_EXTRA_BODY)) {
				messageText = intent.getStringExtra(MessageSendingService.INTENT_EXTRA_BODY);
			}
			if(intent.hasExtra(MessageSendingService.INTENT_EXTRA_ATTCHMENT_URI)) {
				attachmentUri = intent.getStringExtra(MessageSendingService.INTENT_EXTRA_ATTCHMENT_URI);	
			}
		}
		
		etMessageBody.setText(messageText);
		setAttachment(attachmentUri);
	}

	public void onClick(View v) {
		if(v == btSendMessage) {
			final String Message = etMessageBody.getText().toString();
			if(Message.length() < 2) {
				Toast.makeText(this, R.string.Enter_a_message, Toast.LENGTH_SHORT).show();
				return;
			}
			
			Intent sending = new Intent(this, MessageSendingService.class);
			sending.putExtra(MessageSendingService.INTENT_EXTRA_BODY, Message);
			if(attachmentUri != null) {
				sending.putExtra(MessageSendingService.INTENT_EXTRA_ATTCHMENT_URI, attachmentUri);
			}
			startService(sending);
			finish();
		} else if(v == btAttach) {
			if(attachmentUri == null) {
				AlertDialog.Builder builder = new AlertDialog.Builder(this);
				builder.setTitle(R.string.Attach_title);
				builder.setAdapter(new AttachMenuAdapter(this), this);
				builder.show();
			} else {
				setAttachment(null);
			}
		}
		
	}

	public void onClick(DialogInterface dialog, int which) {
		Intent intent;
		switch (which) {
		case 0:
			intent = new Intent(Intent.ACTION_GET_CONTENT);
			intent.setType("image/jpeg");
			startActivityForResult(Intent.createChooser(intent, null), REQUEST_ATTACH_IMAGE);
			break;
			
		case 1:
			intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
			intent.putExtra(MediaStore.EXTRA_OUTPUT, MediaStore.Images.Media.EXTERNAL_CONTENT_URI.toString());
			startActivityForResult(intent, REQUEST_ATTACH_IMAGE);
			break;
		}
		
	}
	
	private void setAttachment(String attUri) {
		if(attUri == null) {
			attachmentUri = null;
			attachmentMime = null;
			btAttach.setSelected(false);
			btAttach.setImageResource(R.drawable.ic_button_attachment);
		} else {
			attachmentUri = attUri;
			attachmentMime = "image/jpeg";
			btAttach.setSelected(true);
			setAttachmentThumbnail(attUri);
		}
	}
	
	private void setAttachmentThumbnail(String attUri) {
		String proj[] = {MediaStore.Images.Media._ID};
		Cursor cur = MediaStore.Images.Media.query(getContentResolver(), Uri.parse(attUri), proj);
		if(cur != null && cur.getCount() > 0) {
			cur.moveToFirst();
			int imageId = cur.getInt( cur.getColumnIndex(MediaStore.Images.Media._ID) );
			cur.close();
			
			Bitmap thumb = MediaStore.Images.Thumbnails.getThumbnail(getContentResolver(), imageId, MediaStore.Images.Thumbnails.MICRO_KIND, null);
			btAttach.setImageBitmap(thumb);
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if(resultCode == RESULT_OK) {
			if(requestCode == REQUEST_ATTACH_IMAGE && data != null) {
				setAttachment(data.getDataString());
			}
		}
	}
}


class AttachMenuAdapter extends BaseAdapter {
	private Context context;
	private final int icons[] = {
		R.drawable.ic_attach_photo,
		R.drawable.ic_attach_photo_new
	};
	private final int labels[] = {
			R.string.Photo_from_gallery,
			R.string.New_photo
	};
	
	public AttachMenuAdapter(Context context) {
		super();
		this.context = context;
	}

	public int getCount() {
		return labels.length;
	}

	public Object getItem(int pos) {
		if(pos >= 0 && pos < getCount())
			return context.getResources().getString(labels[pos]);
		else
			return "";
	}

	public long getItemId(int pos) {
		return pos;
	}

	public View getView(int pos, View convertView, ViewGroup parent) {
		TextView tv = (TextView) convertView;
		if(tv == null) {
			LayoutInflater li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			tv = (TextView) li.inflate(android.R.layout.simple_list_item_1, null);
			tv.setTextColor(Color.BLACK);
			tv.setCompoundDrawablePadding(10);
		}
		
		if(pos >= 0 && pos < getCount()) {
			tv.setText(labels[pos]);
			tv.setCompoundDrawablesWithIntrinsicBounds(icons[pos], 0, 0, 0);
		}
		return tv;
	}
	
}
