package com.mutineer.kissel;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.text.style.UnderlineSpan;
import android.util.Base64;
import android.util.Log;

public class Utils {
	public static boolean hasAuth(Context context)
	{
		AccountManager am = AccountManager.get(context);
		Account accs[] = am.getAccountsByType(context.getString(R.string.account_type));
		return accs.length == 1;
	}
	
	public static String getAuthToken(Context context)
	{
		AccountManager am = AccountManager.get(context);
		Account accs[] = am.getAccountsByType(context.getString(R.string.account_type));
		if(accs.length == 1)
		{
			String authStr = accs[0].name + ":" + am.getPassword(accs[0]);
			return "Basic " + Base64.encodeToString(authStr.getBytes(), Base64.NO_WRAP);
		}
		else
		{
			return "";
		}
	}
	
	public static String getJSON(Context context, String url)
	{
		String ret = null;
		try
		{
			URL jsonURL = new URL(url);
			HttpURLConnection conn = (HttpURLConnection) jsonURL.openConnection();
			
			String basicAuth = getAuthToken(context);
			if(basicAuth.length() != 0)
				conn.setRequestProperty("Authorization", basicAuth);
			
			conn.setUseCaches(true);
			conn.setDoInput(true);
			conn.connect();
			if(conn.getResponseCode() == 200)
				ret = streamToString(conn.getInputStream());
			
			conn.disconnect();
		}
		catch (Exception e)
		{
			Log.e("getJSON", e.toString());
		}
		return ret;
	}
	
	public static String postJSON(Context context, String url, String data) {
		String ret = null;
		try {
			URL jsonURL = new URL(url);
			HttpURLConnection conn = (HttpURLConnection) jsonURL.openConnection();
			
			String basicAuth = getAuthToken(context);
			if(basicAuth.length() != 0)
				conn.setRequestProperty("Authorization", basicAuth);
			
			conn.setUseCaches(true);
			conn.setDoInput(true);
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");
			conn.connect();
			
			OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
			wr.write(data);
			wr.close();
			
			if(conn.getResponseCode() == 200)
				ret = streamToString(conn.getInputStream());
			
			conn.disconnect();
		} catch (Exception e) {
			Log.e("postJSON", e.toString());
		}
		return ret;
	}
	
	public static String streamToString(InputStream stream)
	{
		try
		{
			BufferedReader buf = new BufferedReader(new InputStreamReader(stream));
			StringBuilder str = new StringBuilder();
			String line;
			
			do
			{
				line = buf.readLine();
				str.append(line + "\n");
			} while(line != null);
			return str.toString();
		}
		catch (Exception e)
		{
			Log.e("streamToString", e.toString());
		}
		return null;
	}
	
	public static void startCheckingUpdatesWithPreferedInterval(Context context) {
		SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
		
		String interval =  sp.getString(context.getString(R.string.Prefs_id_refresh), "0");
		long intervalMsecs = Long.parseLong(interval) * 60 * 1000;
		if(intervalMsecs > 0)
			startCheckingUpdates(context, intervalMsecs);
	}
	
	public static void startCheckingUpdates(Context context, long interval) {
		Intent intent = new Intent(context, HomeUpdateService.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		
		AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
		am.setInexactRepeating(AlarmManager.RTC_WAKEUP, 
							   System.currentTimeMillis() + interval / 2, 
							   interval, 
							   PendingIntent.getService(context, 0, intent, 0));
		
		Log.d("Updates", "start " + String.valueOf(interval));
	}
	
	public static void stopCheckingUpdates(Context context) {
		Intent intent = new Intent(context, HomeUpdateService.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		
		AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
		am.cancel(PendingIntent.getService(context, 0, intent, 0));
		
		Log.d("Updates", "stop");
	}
	
	public static CharSequence highlightLinksInText(String text, String[] links) {
		CharSequence formattedText = text;
		if(links.length > 0 && !links[0].equals("")) {
			SpannableString ts = new SpannableString(text);
			String[] positions;
			for(String link : links) {
				positions = link.split(":");
				ts.setSpan(new UnderlineSpan(), Integer.parseInt(positions[0]), 
												Integer.parseInt(positions[1]), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
				ts.setSpan(new ForegroundColorSpan(0xFF0000CC), Integer.parseInt(positions[0]), 
																Integer.parseInt(positions[1]), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
			}
			formattedText = ts;
		}
		return formattedText;
	}
}
