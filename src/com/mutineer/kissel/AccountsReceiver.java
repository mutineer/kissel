package com.mutineer.kissel;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;
import android.util.Log;

public class AccountsReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		if(!Utils.hasAuth(context)) {
			context.deleteDatabase(DBHelper.DB_NAME);
			
			Editor edit = PreferenceManager.getDefaultSharedPreferences(context).edit();
			edit.clear();
			edit.commit();
			
			Utils.stopCheckingUpdates(context);
			Log.d("AccountsReceiver", "Account removed");
		}
	}

}
