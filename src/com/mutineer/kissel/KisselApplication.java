package com.mutineer.kissel;

import android.app.Application;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;

public class KisselApplication extends Application {
	public static final int NOTIFICATION_SEND_FAILS = 0;
	public static final int NOTIFICATION_NEW_MESSAGES = 1;
	public static final int NOTIFICATION_SENDING = 2;
	
	static final String EXTRA_MID = "mid";
	static final String EXTRA_NEW_MESSAGES_COUNT = "NewMessagesCount";
	
	static final String ACTION_NEW_MESSAGES = "com.mutineer.kissel.NEW_MESSAGES";
	static final String ACTION_UPDATE_IN_PROGRESS = "com.mutineer.kissel.UPDATE_IN_PROGRESS";
	
	private static final String NEW_MESSAGES_NUM_KEY = "NewMessagesNum";
	
	private boolean mbMainActivityActive = false;
	private DBHelper helper = null;
	private NotificationManager nm = null;
	private SharedPreferences sp = null;

	public synchronized void setMainActivityStatus(boolean bActive) {
		mbMainActivityActive = bActive;
		if(mbMainActivityActive) {
			if(nm == null)
				nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
			if(sp == null)
				sp = PreferenceManager.getDefaultSharedPreferences(this);
			
			nm.cancel(NOTIFICATION_NEW_MESSAGES);
			Editor editor = sp.edit();
			editor.putInt(NEW_MESSAGES_NUM_KEY, 0);
			editor.commit();
		}
	}
	
	public synchronized boolean isMainActivityActive() {
		return mbMainActivityActive;
	}
	
	public void notifyNewMessages(int number) {
		if(sp == null)
			sp = PreferenceManager.getDefaultSharedPreferences(this);
		
		if(!mbMainActivityActive && sp.getBoolean(getString(R.string.prefs_id_notify_msg), true)) {
			number += sp.getInt(NEW_MESSAGES_NUM_KEY, 0);
			Editor editor = sp.edit();
			editor.putInt(NEW_MESSAGES_NUM_KEY, number);
			editor.commit();
			
			Intent newMessagesIntent = new Intent(this, MainActivity.class);
			newMessagesIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			
			Notification notification = new Notification(R.drawable.ic_stat_notify_new_message, 
														 getString(R.string.New_juick_messages), 
														 System.currentTimeMillis());
			notification.setLatestEventInfo(this, 
											getString(R.string.New_juick_messages), 
											null, 
											PendingIntent.getActivity(this, 0, newMessagesIntent, 0));
			notification.number = number;
			notification.flags |= Notification.FLAG_AUTO_CANCEL;
			notification.defaults |= Notification.DEFAULT_VIBRATE;
			notification.defaults |= Notification.DEFAULT_LIGHTS;
			notification.flags |= Notification.FLAG_SHOW_LIGHTS;
			
			if(nm == null)
				nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
			nm.notify(NOTIFICATION_NEW_MESSAGES, notification);
		}
	}
	
	public synchronized DBHelper getDBHelper() {
		if(helper == null)
			helper = new DBHelper(this);
		return helper;
	}
}
