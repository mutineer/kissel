package com.mutineer.kissel;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.TimeZone;

import org.json.JSONArray;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.StyleSpan;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.mutineer.kissel.api.JuickMessage;

public class ThreadActivity extends ListActivity implements OnClickListener, OnItemClickListener {
	private int mid;
	private int rid;
	private String Replies;
	
	private View vHeader;
	private TextView tvRepliesNum;
	private Button btSend;
	private EditText etBody;
	private TextView tvReplyTo;
	
	private ThreadAdapter adapter;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	
	    Intent intent = getIntent();
	    mid = intent.getIntExtra(KisselApplication.EXTRA_MID, 0);
	    if(mid == 0) {
	    	finish();
	    }
	    
	    Replies = getString(R.string.Replies) + " ";
	    
	    setContentView(R.layout.thread);
	    btSend = (Button) findViewById(R.id.buttonSend);
	    etBody = (EditText) findViewById(R.id.editMessageBody);
	    tvReplyTo = (TextView) findViewById(R.id.textReplyTo);
	    
	    btSend.setOnClickListener(this);
	    
	    initAdapter();
	    new ThreadLoader().execute();
	}
	
	private void initAdapter() {
		vHeader = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.thread_header, null);
	    getListView().addHeaderView(vHeader);
	    
	    tvRepliesNum = (TextView) vHeader.findViewById(R.id.RepliesNum);
	    
	    adapter = new ThreadAdapter(this);
	    getListView().setAdapter(adapter);
	    
	    getListView().setOnItemClickListener(this);
	}
	
	private void setRepliesNumber() {
		tvRepliesNum.setText(Replies + adapter.getCount());
	}
	
	private void fillHeader(JuickMessage msg) {
		final TextView tvTitle = (TextView) findViewById(android.R.id.title);
		tvTitle.setText("@" + msg.UserName + " #" + mid);
		
		final TextView tvTags = (TextView) vHeader.findViewById(R.id.Tags);
		if(!msg.Tags.equals("")) {
			tvTags.setVisibility(View.VISIBLE);
			tvTags.setText(msg.Tags);
		}
		
		final TextView tvPhoto = (TextView) vHeader.findViewById(R.id.Photo);
		if(!msg.Photo.equals("")) {
			tvPhoto.setVisibility(View.VISIBLE);
			SpannableString photoUrl = new SpannableString(msg.Photo);
			photoUrl.setSpan(new UnderlineSpan(), 0, msg.Photo.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
			tvPhoto.setText(photoUrl);
		}
		
		final TextView tvText = (TextView) vHeader.findViewById(R.id.Text);
		tvText.setText(Utils.highlightLinksInText(msg.Text, msg.Links.split(" ")));
		
		final TextView tvTimestamp = (TextView) vHeader.findViewById(R.id.Timestamp);
		DateFormat df = new SimpleDateFormat("HH:mm dd/MMM/yy");
        df.setTimeZone(TimeZone.getDefault());
		tvTimestamp.setText(df.format(msg.Timestamp));
	}

	public void onClick(View v) {
		if(v == btSend) {
			String reply = etBody.getText().toString();
			if(reply.length() < 3) {
				Toast.makeText(this, R.string.Enter_a_message, Toast.LENGTH_SHORT).show();
				return;
			}
			
			String msgNum = "#" + mid;
			if(rid > 0)
				msgNum += "/" + rid;
			
			final String body = msgNum + " " + reply;
			
			etBody.setEnabled(false);
			btSend.setEnabled(false);
			
			Thread thr = new Thread(new Runnable() {
				public void run() {
					try {
						final String ret = Utils.postJSON(ThreadActivity.this, "http://api.juick.com/post", "body=" + URLEncoder.encode(body, "utf-8"));
					
						ThreadActivity.this.runOnUiThread(new Runnable() {
							public void run() {
								etBody.setEnabled(true);
								btSend.setEnabled(true);
								if(ret == null) {
									Toast.makeText(ThreadActivity.this, R.string.Error, Toast.LENGTH_SHORT).show();
								} else {
									Toast.makeText(ThreadActivity.this, R.string.Message_posted, Toast.LENGTH_SHORT).show();
									rid = 0;
									tvReplyTo.setVisibility(View.GONE);
									etBody.setText("");
								}
							}
						});
					} catch (UnsupportedEncodingException e) {
						e.printStackTrace();
					}
				}
			});
			thr.start();
		}
	}
	
	public void onItemClick(AdapterView<?> parent, View view, int pos, long id) {
		int headersNum = getListView().getHeaderViewsCount();
		if(pos < headersNum) {
			rid = 0;
			tvReplyTo.setVisibility(View.GONE);
		} else {
			JuickMessage msg = (JuickMessage) adapter.getItem(pos - headersNum);
			SpannableStringBuilder ssb = new SpannableStringBuilder();
            String inreplyto = getResources().getString(R.string.In_reply_to) + " ";
            ssb.append(inreplyto + msg.Text);
            ssb.setSpan(new StyleSpan(android.graphics.Typeface.BOLD), 0, inreplyto.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            tvReplyTo.setText(ssb);
            tvReplyTo.setVisibility(View.VISIBLE);
            rid = msg.RID;
		}
	}
	
	class ThreadLoader extends AsyncTask<Void, Void, ArrayList<JuickMessage>> {

		@Override
		protected ArrayList<JuickMessage> doInBackground(Void... params) {
			DBHelper helper = ((KisselApplication) getApplication()).getDBHelper();
			ArrayList<JuickMessage> messages = helper.getReplies(mid);
			
			if(messages.isEmpty()) {
				final String threadJSON = Utils.getJSON(ThreadActivity.this, "http://api.juick.com/thread?mid=" + mid);
				try {
					JSONArray array = new JSONArray(threadJSON);
					int num = array.length();
					for(int i = 0; i < num; i++) {
						messages.add(JuickMessage.parseJSON(array.getJSONObject(i)));
					}
				}
				catch (Exception e) {
					Log.d("ThreadActivity", e.toString());
				}
				
				Collections.sort(messages, new Comparator<JuickMessage>() {
					public int compare(JuickMessage lhs, JuickMessage rhs) {
						if(lhs.RID < rhs.RID) return -1;
						else if(lhs.RID > rhs.RID) return 1;
						else return 0;
					}
				});
			}
			
			return messages;
		}

		@Override
		protected void onPostExecute(ArrayList<JuickMessage> result) {
			ThreadActivity.this.findViewById(R.id.loadingProgress).setVisibility(View.GONE);
			ThreadActivity.this.getListView().setVisibility(View.VISIBLE);
			
			if(result.isEmpty()) {
				ThreadActivity.this.getListView().removeHeaderView(vHeader);
				btSend.setEnabled(false);
				etBody.setEnabled(false);
				
				AlertDialog.Builder builder = new AlertDialog.Builder(ThreadActivity.this);
				builder.setMessage(R.string.Wrong_post_number);
				builder.setNeutralButton(R.string.OK, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
					}
				});
				builder.show();
				return;
			}
			
			for (JuickMessage msg : result) {
				if(msg.RID == 0) {
					ThreadActivity.this.fillHeader(msg);
				} else {
					ThreadActivity.this.adapter.add(msg);
				}
			}
			
			setRepliesNumber();
		}
		
	}

}

class ThreadAdapter extends ArrayAdapter<JuickMessage> {
	private DateFormat df;
	
	public ThreadAdapter(Context context) {
		super(context, 0);
		
		df = new SimpleDateFormat("HH:mm dd/MMM/yy");
        df.setTimeZone(TimeZone.getDefault());
	}
	
	static class ViewHolder
	{
		public TextView mUser;
		public TextView mPhoto;
		public TextView mText;
		public TextView mTimestamp;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		JuickMessage msg = getItem(position);
		
		if(convertView == null)
			convertView = createView(parent);
		
		ViewHolder holder = (ViewHolder) convertView.getTag();
		holder.mUser.setText("@" + msg.UserName);
		
		if(msg.Photo.equals("")) {
			holder.mPhoto.setVisibility(View.GONE);
			holder.mPhoto.setText(0);
		} else {
			holder.mPhoto.setVisibility(View.VISIBLE);
			SpannableString photoUrl = new SpannableString(msg.Photo);
			photoUrl.setSpan(new UnderlineSpan(), 0, msg.Photo.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
			holder.mPhoto.setText(photoUrl);
		}
		
		holder.mText.setText(Utils.highlightLinksInText(msg.Text, msg.Links.split(" ")));
		
		holder.mTimestamp.setText(df.format(msg.Timestamp));
		
		return convertView;
	}
	
	private View createView(ViewGroup parent) {
		ViewHolder holder = new ViewHolder();
		LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View newView = inflater.inflate(R.layout.reply_row, null);
		
		holder.mUser 	  = (TextView) newView.findViewById(R.id.Username);
		holder.mPhoto 	  = (TextView) newView.findViewById(R.id.Photo);
		holder.mText 	  = (TextView) newView.findViewById(R.id.Text);
		holder.mTimestamp = (TextView) newView.findViewById(R.id.Timestamp);
		newView.setTag(holder);
		
		return newView;
	}
}
