package com.mutineer.kissel;

import java.util.ArrayList;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;

import com.mutineer.kissel.api.JuickMessage;

public class MessageMenu implements OnItemLongClickListener, OnClickListener {
	Context context;
	JuickMessage message;
	ArrayList<String> urls;
	
	public MessageMenu(Context context) {
		this.context = context;
		urls = new ArrayList<String>();
	}

	public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
		Cursor cursor = (Cursor) parent.getItemAtPosition(position);
		message = DBHelper.getMessage(cursor);
		
		urls.clear();
		if(!message.Photo.equals("")) {
			urls.add(message.Photo);
		}
		
		String[] links = message.Links.split(" ");
		if(links.length > 0 && !links[0].equals("")) {
			String[] positions;
			for(String link : links) {
				positions = link.split(":");
				urls.add(message.Text.substring(Integer.parseInt(positions[0]), 
												Integer.parseInt(positions[1])));
			}
		}

		int numItems = 4 + urls.size();
		CharSequence[] items = new CharSequence[numItems];

		int i = 0;
		for(String url : urls) {
			items[i++] = url;
		}
		items[i++] = context.getString(R.string.Recommend_message);
		items[i++] = context.getString(message.notFromFriends ? R.string.Subscribe_to : R.string.Unsubscribe_from) + " @" + message.UserName;
		items[i++] = context.getString(R.string.Blacklist) + " @" + message.UserName;
		items[i++] = context.getString(R.string.Share);
		
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setItems(items, this);
		builder.show();
		
		return true;
	}
	
	public void onClick(DialogInterface dialog, int which) {
		Intent intent;
		
		if(which < urls.size()) {
			String url = urls.get(which);
			if(url.startsWith("#")) {
				intent = new Intent(context, ThreadActivity.class);
				intent.putExtra(KisselApplication.EXTRA_MID, Integer.parseInt(url.substring(1)));
				context.startActivity(intent);
			} else {
				context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse( url )));
			}
		}
		which -= urls.size();
		
		switch (which) {
		case 0:
			intent = new Intent(context, MessageSendingService.class);
			intent.putExtra(MessageSendingService.INTENT_EXTRA_BODY, "! #" + message.MID);
			intent.putExtra(MessageSendingService.INTENT_EXTRA_SUCCESS_MESSAGE, R.string.Recommended);
			context.startService(intent);
			break;
			
		case 1:
			intent = new Intent(context, MessageSendingService.class);
			intent.putExtra(MessageSendingService.INTENT_EXTRA_BODY, (message.notFromFriends ? "S @" : "U @") + message.UserName);
			intent.putExtra(MessageSendingService.INTENT_EXTRA_SUCCESS_MESSAGE, message.notFromFriends ? R.string.Subscribed : R.string.Unsubscribed);
			context.startService(intent);
			break;
			
		case 2:
			intent = new Intent(context, MessageSendingService.class);
			intent.putExtra(MessageSendingService.INTENT_EXTRA_BODY, "BL @" + message.UserName);
			intent.putExtra(MessageSendingService.INTENT_EXTRA_SUCCESS_MESSAGE, R.string.Added_to_BL);
			context.startService(intent);
			break;
			
		case 3:
			intent = new Intent(Intent.ACTION_SEND);
			intent.setType("text/plain");
			intent.putExtra(Intent.EXTRA_TEXT, message.toString());
			context.startActivity(Intent.createChooser(intent, null));
			break;
		}
	}
}
