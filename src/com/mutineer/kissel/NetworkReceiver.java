package com.mutineer.kissel;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.preference.PreferenceManager;

public class NetworkReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		boolean isNetworkDown = intent.getBooleanExtra(ConnectivityManager.EXTRA_NO_CONNECTIVITY, false);
		if(isNetworkDown) {
			Utils.stopCheckingUpdates(context);
		} else {
			Utils.startCheckingUpdatesWithPreferedInterval(context);
		}
	}

}
